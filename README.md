# Keepass X Rclone

C'est un script qui permet de lancer Keepass avec un drive gérer par Rclone.

Il utilise une version spécifique de Keepass qui est KeepassXC en version Flatpak([flathub](https://flathub.org/apps/details/org.keepassxc.KeePassXC)).

Le script est écrit en bash.

![Shell Script](https://img.shields.io/badge/shell_script-%23121011.svg?style=for-the-badge&logo=gnu-bash&logoColor=white)

## Config

Le fichier de configuration est situé : `~/.Config/keepass-x-rclone/config`

```Shell
# config file 
KEEPASS_FILE=
REMOTE=
KEYFILE=
REMOTE_BACKUP=
```

- `KEEPASS_FILE` : Nom du fichier keepass. (exemple: `MesMdp.kdbx`)
- `REMOTE` : Drive sur rclone. (exemple: `Mondrive:/monchemin/vers/le/dossier/`)
- `KEYFILE` : Fichier clé si il existe. (exemple: `maclee.keyx`)
- `REMOTE_BACKUP` : Sauvegarde sur un autre drive. (exemple: même chose que
   `REMOTE` fonctionne aussi en local dans ce cas la on met juste le chemin : `~/monchemin/`)

## Licence

GPL 3
